﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService
{
    public class DBManager
    {
        public static SqlSugarClient GetInstance()
        {
            var dbType = SqlSugar.DbType.MySql;
            var connection = AppSettings.Configuration["DBConfig:AzureProgramerTestDB:Connection"];
            var myDBType = AppSettings.Configuration["DBConfig:AzureProgramerTestDB:DBType"];
            if (myDBType == "SqlServer")
            {
                dbType = SqlSugar.DbType.SqlServer;
            }
            var connectionConfig = new ConnectionConfig()
            {
                ConnectionString = connection,
                DbType = dbType,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute
            };
            connectionConfig.AopEvents = AopEvents;
            var sugarClient = new SqlSugarClient(connectionConfig);
            return sugarClient;
        }

        public static void BeginTran()
        {
            var instance = GetInstance();
            instance.BeginTran();
        }

        public static void CommitTran()
        {
            var instance = GetInstance();
            instance.CommitTran();
        }

        public static void RollbackTran()
        {
            var instance = GetInstance();
            instance.RollbackTran();
        }

        public static AopEvents AopEvents
        {
            get
            {
                var aopEvents = new AopEvents();
                aopEvents.OnLogExecuted = (sql, pars) => //SQL executing event (pre-execution)
                {

                };
                aopEvents.OnError = (exp) =>//SQL execution error event
                {

                };
                return aopEvents;
            }
        }
    }
}
