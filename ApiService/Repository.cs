﻿using ApiService.Interface;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ApiService
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        public SqlSugarClient sqlSugarClient;

        //public Repository(DBConfigSetting dataBaseConfig = null)
        //{
        //    if (dataBaseConfig != null)
        //    {
        //        sqlSugarClient = DBManager.GetInstance(dataBaseConfig);
        //    }
        //    else
        //    {
        //        sqlSugarClient = DBManager.GetInstance(DBConfig.BlogDBConfig);
        //    }
        //}

        public Repository()
        {
            //sqlSugarClient = DbScoped.Sugar;
            sqlSugarClient = DBManager.GetInstance();
            //var configId = typeof(T).GetCustomAttribute<TenantAttribute>().configId;
            //sqlSugarClient.ChangeDatabase(configId);
        }

        public SqlSugarClient GetDBContext()
        {
            return sqlSugarClient;
        }

        public List<T> GetAllList()
        {
            return GetAllList(0);
        }

        public List<T> GetAllList(int cacheSeconds = 0)
        {
            return GetAllList(null, cacheSeconds);
        }

        public List<T> GetAllList(Expression<Func<T, bool>> exp = null, int cacheSeconds = 0)
        {
            if (exp == null)
            {
                return sqlSugarClient.Queryable<T>().WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
            }
            else
            {
                return sqlSugarClient.Queryable<T>().Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
            }
        }

        public List<T> GetAllList(string order, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0)
        {
            if (!String.IsNullOrEmpty(order))
            {
                if (exp == null)
                {
                    return sqlSugarClient.Queryable<T>().WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToList();
                }
                else
                {
                    return sqlSugarClient.Queryable<T>().Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToList();
                }
            }
            else
            {
                if (exp == null)
                {
                    return sqlSugarClient.Queryable<T>().ToList();
                }
                else
                {
                    return sqlSugarClient.Queryable<T>().Where(exp).ToList();
                }
            }
        }

        public List<T> GetAllList(int top, string order, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0)
        {
            if (!String.IsNullOrEmpty(order))
            {
                if (exp == null)
                {
                    if (top > 0)
                        return sqlSugarClient.Queryable<T>().WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).Take(top).ToList();
                    else
                        return sqlSugarClient.Queryable<T>().WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).Take(top).ToList();
                }
                else
                {
                    if (top > 0)
                        return sqlSugarClient.Queryable<T>().Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).Take(top).ToList();
                    else
                        return sqlSugarClient.Queryable<T>().Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToList();

                }
            }
            else
            {
                if (exp == null)
                {
                    if (top > 0)
                        return sqlSugarClient.Queryable<T>().Take(top).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
                    else
                        return sqlSugarClient.Queryable<T>().WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
                }
                else
                {
                    if (top > 0)
                        return sqlSugarClient.Queryable<T>().Where(exp).Take(top).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
                    else
                        return sqlSugarClient.Queryable<T>().Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();

                }
            }
        }

        public List<T> GetAllList<FieldType>(Expression<Func<T, bool>> exp, Expression<Func<T, object>> exp2, List<FieldType> inValues, int cacheSeconds = 0)
        {
            return sqlSugarClient.Queryable<T>().Where(exp).In(exp2, inValues).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
        }

        public List<T2> GetAllList<T2>(string sql, int cacheSeconds = 0) where T2 : class, new()
        {
            var list = new List<T2>();
            if (!String.IsNullOrEmpty(sql))
            {
                list = sqlSugarClient.SqlQueryable<T2>(sql).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
            }
            return list;
        }

        public List<T2> GetAllList<T2>(Expression<Func<T, bool>> exp = null, int cacheSeconds = 0) where T2 : class, new()
        {
            if (exp == null)
            {
                return sqlSugarClient.Queryable<T>().Select(t => new T2()).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
            }
            else
            {
                return sqlSugarClient.Queryable<T>().Where(exp).Select(t => new T2()).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
            }
        }

        public List<T2> GetAllList<T2>(Expression<Func<T, T2>> func, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0)
        {
            if (exp == null)
            {
                return sqlSugarClient.Queryable<T>().Select(func).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
            }
            else
            {
                return sqlSugarClient.Queryable<T>().Where(exp).Select(func).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
            }
        }

        public List<T2> GetAllList<T2>(Expression<Func<T, T2>> func, Action<T2> action, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0)
        {
            if (exp == null)
            {
                return sqlSugarClient.Queryable<T>().Select(func).Mapper(action).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
            }
            else
            {
                return sqlSugarClient.Queryable<T>().Where(exp).Select(func).Mapper(action).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
            }
        }

        public List<T> GetAllListIn<FieldType>(Expression<Func<T, bool>> exp, string InFieldName, List<FieldType> inValues, int cacheSeconds = 0)
        {
            return sqlSugarClient.Queryable<T>().Where(exp).In(InFieldName, inValues).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToList();
        }

        public List<T> GetPageList(int pageindex, int pagesize, string order, out int total, int cacheSeconds = 0)
        {
            return GetPageList(pageindex, pagesize, order, out total, null, cacheSeconds);
        }

        public List<T> GetPageList(int pageindex, int pagesize, string order, out int total, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0)
        {
            total = 0;
            if (exp == null)
            {
                if (pageindex < 1) pageindex = 1;
                //total = sqlSugarClient.Queryable<T>().Count();
                if (String.IsNullOrEmpty(order))
                {
                    //return sqlSugarClient.Queryable<T>().Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    return list;
                }
                else
                {
                    //return sqlSugarClient.Queryable<T>().OrderBy(order).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().OrderBy(order).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    return list;
                }
            }
            else
            {
                if (pageindex < 1) pageindex = 1;
                //total = sqlSugarClient.Queryable<T>().Where(exp).Count();
                if (String.IsNullOrEmpty(order))
                {
                    //return sqlSugarClient.Queryable<T>().Where(exp).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    return list;
                }
                else
                {
                    //return sqlSugarClient.Queryable<T>().Where(exp).OrderBy(order).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToPageList(pageindex, pagesize, ref total);
                    return list;
                }
            }
        }

        public List<T> GetPageList(int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0)
        {
            pagecount = 0;
            total = 0;
            if (exp == null)
            {
                if (pageindex < 1) pageindex = 1;
                //total = sqlSugarClient.Queryable<T>().Count();
                //pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                if (String.IsNullOrEmpty(order))
                {
                    //return sqlSugarClient.Queryable<T>().Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
                else
                {
                    //return sqlSugarClient.Queryable<T>().OrderBy(order).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
            }
            else
            {
                if (pageindex < 1) pageindex = 1;
                //total = sqlSugarClient.Queryable<T>().Count(exp);
                //pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                if (String.IsNullOrEmpty(order))
                {
                    //return sqlSugarClient.Queryable<T>().Where(exp).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
                else
                {
                    //return sqlSugarClient.Queryable<T>().Where(exp).OrderBy(order).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
            }
        }

        public List<T2> GetPageList<T2>(string sql, int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T2, bool>> exp = null, int cacheSeconds = 0) where T2 : class, new()
        {
            pagecount = 0;
            total = 0;
            if (exp == null)
            {
                if (pageindex < 1) pageindex = 1;
                if (String.IsNullOrEmpty(order))
                {
                    var list = sqlSugarClient.SqlQueryable<T2>(sql).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
                else
                {
                    var list = sqlSugarClient.SqlQueryable<T2>(sql).WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
            }
            else
            {
                if (pageindex < 1) pageindex = 1;
                if (String.IsNullOrEmpty(order))
                {
                    var list = sqlSugarClient.SqlQueryable<T2>(sql).Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
                else
                {
                    var list = sqlSugarClient.SqlQueryable<T2>(sql).Where(exp).WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
            }
        }

        public List<T2> GetPageList<T2>(int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T, T2>> func, int cacheSeconds = 0)
        {
            return GetPageList(pageindex, pagesize, order, out total, out pagecount, func, null, cacheSeconds);
        }

        public List<T2> GetPageList<T2>(int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T, T2>> func, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0)
        {
            total = 0;
            if (exp == null)
            {
                if (pageindex < 1) pageindex = 1;
                //total = sqlSugarClient.Queryable<T>().Count();
                if (String.IsNullOrEmpty(order))
                {
                    //return sqlSugarClient.Queryable<T>().Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Select(func).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
                else
                {
                    //return sqlSugarClient.Queryable<T>().OrderBy(order).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Select(func).WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
            }
            else
            {
                if (pageindex < 1) pageindex = 1;
                //total = sqlSugarClient.Queryable<T>().Where(exp).Count();
                if (String.IsNullOrEmpty(order))
                {
                    //return sqlSugarClient.Queryable<T>().Where(exp).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Where(exp).Select(func).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
                else
                {
                    //return sqlSugarClient.Queryable<T>().Where(exp).OrderBy(order).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Where(exp).Select(func).WithCacheIF(cacheSeconds > 0, cacheSeconds).OrderBy(order).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
            }
        }

        public List<T2> GetPageList<T2>(int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T, T2>> func, Action<T2> action, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0)
        {
            total = 0;
            if (exp == null)
            {
                if (pageindex < 1) pageindex = 1;
                //total = sqlSugarClient.Queryable<T>().Count();
                if (String.IsNullOrEmpty(order))
                {
                    //return sqlSugarClient.Queryable<T>().Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Select(func).Mapper(action).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
                else
                {
                    //return sqlSugarClient.Queryable<T>().OrderBy(order).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Select(func).Mapper(action).OrderBy(order).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
            }
            else
            {
                if (pageindex < 1) pageindex = 1;
                //total = sqlSugarClient.Queryable<T>().Where(exp).Count();
                if (String.IsNullOrEmpty(order))
                {
                    //return sqlSugarClient.Queryable<T>().Where(exp).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Where(exp).Select(func).Mapper(action).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
                else
                {
                    //return sqlSugarClient.Queryable<T>().Where(exp).OrderBy(order).Skip(pagesize * (pageindex - 1)).Take(pagesize).ToList();
                    var list = sqlSugarClient.Queryable<T>().Where(exp).Select(func).Mapper(action).OrderBy(order).WithCacheIF(cacheSeconds > 0, cacheSeconds).ToPageList(pageindex, pagesize, ref total);
                    pagecount = total % pagesize == 0 ? total / pagesize : total / pagesize + 1;
                    return list;
                }
            }
        }

        public object GetScalar(string sql, params SugarParameter[] parameters)
        {
            if (!String.IsNullOrEmpty(sql))
            {
                return sqlSugarClient.Ado.GetScalar(sql);
            }

            return default;
        }

        public T Get(object id)
        {
            return sqlSugarClient.Queryable<T>().InSingle(id);
        }

        public object Insert(T entity, SqlSugarClient dbClient = null)
        {
            if (dbClient != null)
            {
                return dbClient.Insertable(entity).ExecuteCommand();
            }
            else
            {
                return sqlSugarClient.Insertable(entity).ExecuteCommand();
            }
        }

        public object InsertRange(List<T> entity, SqlSugarClient dbClient = null)
        {
            if (dbClient != null)
            {
                return dbClient.Insertable(entity).ExecuteCommand();
            }
            else
            {
                return sqlSugarClient.Insertable(entity).ExecuteCommand();
            }
        }

        public bool Update(T entity, SqlSugarClient dbClient = null)
        {
            if (dbClient != null)
            {
                return dbClient.Updateable(entity).ExecuteCommand() > 0;
            }
            else
            {
                return sqlSugarClient.Updateable(entity).ExecuteCommand() > 0;
            }
        }

        public bool Update(T entity, Expression<Func<T, object>> exp, SqlSugarClient dbClient = null)
        {
            if (dbClient != null)
            {
                return dbClient.Updateable(entity).WhereColumns(exp).ExecuteCommand() > 0;
            }
            else
            {
                return sqlSugarClient.Updateable(entity).WhereColumns(exp).ExecuteCommand() > 0;
            }
        }

        /// <summary>
        /// 根据表达式更新局部列
        /// </summary>
        /// <param name="columns">局部列</param>
        /// <param name="exp">表达式</param>
        /// <returns></returns>
        public bool UpdateColumns(Expression<Func<T, T>> columns, Expression<Func<T, bool>> exp, SqlSugarClient dbClient = null)
        {
            if (dbClient != null)
            {
                return dbClient.Updateable<T>().SetColumns(columns).Where(exp).ExecuteCommand() > 0;
            }
            else
            {
                return sqlSugarClient.Updateable<T>().SetColumns(columns).Where(exp).ExecuteCommand() > 0;
            }
        }

        public bool Delete(string id, SqlSugarClient dbClient = null)
        {
            if (dbClient != null)
            {
                return dbClient.Deleteable<T>().In(id).ExecuteCommand() > 0;
            }
            else
            {
                return sqlSugarClient.Deleteable<T>().In(id).ExecuteCommand() > 0;
            }
        }

        public bool DeleteByExp(Expression<Func<T, bool>> exp, SqlSugarClient dbClient = null)
        {
            if (dbClient != null)
            {
                return dbClient.Deleteable<T>().Where(exp).ExecuteCommand() > 0;
            }
            else
            {
                return sqlSugarClient.Deleteable<T>().Where(exp).ExecuteCommand() > 0;
            }
        }

        public T First()
        {
            return sqlSugarClient.Queryable<T>().First();
        }

        public T First(Expression<Func<T, bool>> exp)
        {
            return sqlSugarClient.Queryable<T>().First(exp);
        }

        public T FirstOrDefault()
        {
            return sqlSugarClient.Queryable<T>().Single();
        }

        public T FirstOrDefault(Expression<Func<T, bool>> exp)
        {
            return sqlSugarClient.Queryable<T>().First(exp);
        }

        public T FirstOrDefault(Expression<Func<T, bool>> exp, string orderby)
        {
            return sqlSugarClient.Queryable<T>().OrderBy(orderby).First(exp);
        }

        public T Single()
        {
            return sqlSugarClient.Queryable<T>().First();
        }

        public T Single(Expression<Func<T, bool>> exp)
        {
            return sqlSugarClient.Queryable<T>().First(exp);
        }

        public T SingleOrDefault()
        {
            return sqlSugarClient.Queryable<T>().Single();
        }

        public T SingleOrDefault(Expression<Func<T, bool>> exp)
        {
            return sqlSugarClient.Queryable<T>().Single(exp);
        }

        public bool Any()
        {
            return sqlSugarClient.Queryable<T>().Any();
        }

        public bool Any(Expression<Func<T, bool>> exp)
        {
            return sqlSugarClient.Queryable<T>().Any(exp);
        }

        public int Count(Expression<Func<T, bool>> exp)
        {
            return sqlSugarClient.Queryable<T>().Count(exp);
        }

        public void RemoveDataCache(string likeString)
        {
            sqlSugarClient.DataCache.RemoveDataCache(likeString);
        }
    }
}
