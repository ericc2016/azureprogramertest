﻿using ApiService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Interface
{
    public interface IOderService
    {
        int AddOrder(Order arg);

        List<Order> GetList(string? BuyerName, string? PurchaseOrderNumber, string? BillingZipCode);

        //List<Order> GetList(Order arg);
    }
}
