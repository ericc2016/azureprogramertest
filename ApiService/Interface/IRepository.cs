﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Interface
{
    public interface IRepository<T>/*: IScopedDependency*/ where T : class, new()
    {
        SqlSugarClient GetDBContext();

        List<T> GetAllList();

        List<T> GetAllList(int cacheSeconds = 0);

        List<T> GetAllList(Expression<Func<T, bool>> exp = null, int cacheSeconds = 0);

        List<T> GetAllList(string order, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0);

        List<T> GetAllList(int top, string order, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0);

        List<T2> GetAllList<T2>(string sql, int cacheSeconds = 0) where T2 : class, new();

        List<T2> GetAllList<T2>(Expression<Func<T, bool>> exp = null, int cacheSeconds = 0) where T2 : class, new();

        List<T2> GetAllList<T2>(Expression<Func<T, T2>> func, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0);

        List<T2> GetAllList<T2>(Expression<Func<T, T2>> func, Action<T2> action, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0);

        List<T> GetPageList(int pageindex, int pagesize, string order, out int total, int cacheSeconds = 0);

        List<T> GetPageList(int pageindex, int pagesize, string order, out int total, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0);

        List<T> GetPageList(int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0);

        List<T2> GetPageList<T2>(string sql, int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T2, bool>> exp = null, int cacheSeconds = 0) where T2 : class, new();

        List<T2> GetPageList<T2>(int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T, T2>> func, int cacheSeconds = 0);

        List<T2> GetPageList<T2>(int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T, T2>> func, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0);

        List<T2> GetPageList<T2>(int pageindex, int pagesize, string order, out int total, out int pagecount, Expression<Func<T, T2>> func, Action<T2> action, Expression<Func<T, bool>> exp = null, int cacheSeconds = 0);

        List<T> GetAllList<FieldType>(Expression<Func<T, bool>> exp, Expression<Func<T, object>> exp2, List<FieldType> inValues, int cacheSeconds = 0);

        List<T> GetAllListIn<FieldType>(Expression<Func<T, bool>> exp, string InFieldName, List<FieldType> inValues, int cacheSeconds = 0);

        T Get(object id);

        object Insert(T entity, SqlSugarClient dbClient = null);

        object InsertRange(List<T> entity, SqlSugarClient dbClient = null);

        //object SqlBulkCopy(List<T> entity);

        bool Update(T entity, SqlSugarClient dbClient = null);

        bool Update(T entity, Expression<Func<T, object>> exp, SqlSugarClient dbClient = null);

        bool UpdateColumns(Expression<Func<T, T>> columns, Expression<Func<T, bool>> exp, SqlSugarClient dbClient = null);

        bool Delete(string id, SqlSugarClient dbClient = null);

        bool DeleteByExp(Expression<Func<T, bool>> exp, SqlSugarClient dbClient = null);

        T First();

        T First(Expression<Func<T, bool>> exp);

        T FirstOrDefault();

        T FirstOrDefault(Expression<Func<T, bool>> exp);

        T FirstOrDefault(Expression<Func<T, bool>> exp, string orderby);

        T Single();

        T Single(Expression<Func<T, bool>> exp);

        T SingleOrDefault();

        T SingleOrDefault(Expression<Func<T, bool>> exp);

        bool Any();

        bool Any(Expression<Func<T, bool>> exp);

        object GetScalar(string sql, params SugarParameter[] parameters);

        int Count(Expression<Func<T, bool>> exp);

        void RemoveDataCache(string likeString);
    }
}
