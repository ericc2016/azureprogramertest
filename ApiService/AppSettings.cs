﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService
{
    public class AppSettings
    {
        private static IConfiguration _configuration;

        /// <summary>
        /// 
        /// </summary>
        public static IConfiguration Configuration
        {
            get
            {
                if (_configuration == null)
                {
                    _configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false).Build();
                }
                return _configuration;
            }
            set => _configuration = value;
        }
    }
}
