﻿using ApiService.Entity;
using ApiService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService
{
    public class OderService : IOderService
    {
        private IRepository<Order> _orderRepository;

        public OderService(IRepository<Order> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public int AddOrder(Order arg)
        {
            if (arg == null)
            {
                return 400;
            }
            if (String.IsNullOrEmpty(arg.BillingZipCode))
            {
                return 400;
            }
            if (_orderRepository.Any(t => t.PurchaseOrderNumber.Equals(arg.PurchaseOrderNumber)))
            {
                return 204;
            }
            arg.InsertTime = DateTime.Now;
            var result = _orderRepository.Insert(arg);
            return result != null ? 201 : -1;
        }

        public List<Order> GetList(string? BuyerName, string? PurchaseOrderNumber, string? BillingZipCode)
        {
            return GetList(new Order()
            {
                BuyerName = BuyerName,
                PurchaseOrderNumber = PurchaseOrderNumber,
                BillingZipCode = BillingZipCode
            });
        }

        public List<Order> GetList(Order arg)
        {
            var exp = ExprBuilder.True<Order>();
            if (arg != null)
            {
                if (!String.IsNullOrEmpty(arg.BuyerName))
                {
                    exp = exp.AndExpression(t => t.BuyerName.Equals(arg.BuyerName));
                }
                if (!String.IsNullOrEmpty(arg.PurchaseOrderNumber))
                {
                    exp = exp.AndExpression(t => t.PurchaseOrderNumber.Equals(arg.PurchaseOrderNumber));
                }
                if (!String.IsNullOrEmpty(arg.BillingZipCode))
                {
                    exp = exp.AndExpression(t => t.BillingZipCode.Equals(arg.BillingZipCode));
                }
            }
            var list = _orderRepository.GetAllList(exp, 0);
            return list;
        }
    }
}
