﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Entity
{
    [SugarTable("tb_orders")]
    public class Order
    {
        [SugarColumn(IsPrimaryKey = true)]
        public int Id { get; set; }

        public string BuyerName { get; set; }

        public string PurchaseOrderNumber { get; set; }

        [SugarColumn(IsNullable = false)]
        public string? BillingZipCode { get; set; }

        public double? OrderAmount { get; set; }

        [SugarColumn(IsOnlyIgnoreInsert = true)]
        public DateTime? InsertTime { get; set; }

        [SugarColumn(IsOnlyIgnoreUpdate = true)]
        public DateTime? UpdateTime { get; set; }
    }
}
