﻿using ApiService.Entity;
using ApiService.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/orders/[action]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private IOderService _orderService;

        public OrdersController(IOderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        public void PostOrder([FromBody]Order arg)
        {
            var result = _orderService.AddOrder(arg);
            Response.StatusCode = result;
            //return new NoContentResult();
        }

        [HttpGet]
        public List<Order> GetOrders(string? BuyerName, string? PurchaseOrderNumber, string? BillingZipCode)
        {
            var result = _orderService.GetList(BuyerName, PurchaseOrderNumber, BillingZipCode);
            return result;
        }
    }
}
