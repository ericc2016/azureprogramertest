using ApiService.Entity;
using ApiService.Interface;
using Xunit;

namespace WebApiTest
{
    public class UnitTest
    {
        private IOderService _orderService;

        public UnitTest(IOderService orderService)
        {
            _orderService = orderService;
        }

        [Fact]
        public void Test1()
        {
            var order = new Order()
            {
                BuyerName = "Johnny",
                PurchaseOrderNumber = "003",
                BillingZipCode = "411103",
                OrderAmount = 208.5
            };
            var result = _orderService.AddOrder(order);
            Assert.Equal(201, result);
        }

        [Fact]
        public void Test2()
        {
            var order = new Order()
            {
                BuyerName = "Eric",
                PurchaseOrderNumber = "003",
                BillingZipCode = "411104",
                OrderAmount = 102.6
            };
            var result = _orderService.AddOrder(order);
            Assert.Equal(201, result);
        }

        [Fact]
        public void Test3()
        {
            var order = new Order()
            {
                BuyerName = "Andrew",
                PurchaseOrderNumber = "005",
                //BillingZipCode = "411105",
                OrderAmount = 98.6
            };
            var result = _orderService.AddOrder(order);
            Assert.Equal(201, result);
        }
    }
}