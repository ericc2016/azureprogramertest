﻿using ApiService;
using ApiService.Entity;
using ApiService.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using Xunit.DependencyInjection;

namespace WebApiTest
{
    public class Startup
    {
        public void ConfigureHost(IHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureServices((context, services) =>
                {
                    services.AddScoped<IRepository<Order>, Repository<Order>>();
                    services.AddTransient<IOderService, OderService>();
                });
        }
    }
}
